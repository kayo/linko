#include "common.h"
#include "node.h"
#include "addon.h"

#include <khash.h>

__KHASH_TYPE(nodes, kh_cstr_t, node_t*)

struct _node {
  root_t *root;
  node_t *parent;
  khash_t(nodes) children;
  node_addon_t *addon;
};

struct _root {
  node_t node;
  root_addon_t *addon;
  
  uv_loop_t *uv_loop;
  addons_t *addons;
};

__KHASH_IMPL(nodes, static kh_inline klib_unused, kh_cstr_t, node_t*, 1, kh_str_hash_func, kh_str_hash_equal)

static void node_init(node_t *node, root_t *root, node_t *parent, node_addon_t *addon) {
  node->root = root;
  node->parent = parent;
  
  khash_t(nodes) *h = &node->children;
  
  kh_prepare(nodes, h);
  
  node->addon = addon;
  
  int a;
  for (a = none; none != (a = addons_next(root->addons, a)); ) {
    const addon_vmt_t *vmt = addons_vmt(root->addons, a);
    if (vmt->node_init) vmt->node_init(node, a);
  }
}

node_t *node_add(node_t *parent, const char *name) {
  khash_t(nodes) *h = &parent->children;
  int r;
  khint_t i = kh_put(nodes, h, name, &r);
  
  if (r > 0) {
    kh_key(h, i) = strdup(name);
    
    root_t *root = node_root(parent);
    node_t *node = kh_val(h, i) =
      malloc(sizeof(node_t) + addons_node_size(root->addons));
    
    node_init(node, root, parent,
              (node_addon_t*)((char*)node + sizeof(node_t)));
    
    return node;
  }
  
  return NULL;
}

static void node_rem(node_t *node);

static void node_done(node_t *node) {
  root_t *root = node->root;
  khint_t i;
  khash_t(nodes) *h = &node->children;
  
	for (i = kh_begin(h); i != kh_end(h); i++) {
		if (!kh_exist(h, i)) continue;
    
    node_rem(kh_val(h, i));
    free((char*)kh_key(h, i));
  }
  
  kh_finalize(nodes, h);
  
  int a;
  for (a = none; none != (a = addons_next(root->addons, a)); ) {
    const addon_vmt_t *vmt = addons_vmt(root->addons, a);
    if (vmt->node_done) vmt->node_done(node, a);
  }
}

static void node_rem(node_t *node) {
  node_done(node);
  
  free(node);
}

root_t* root_new(uv_loop_t *uv_loop) {
  addons_t *addons = addons_new();
  root_t *root = malloc(sizeof(root_t) + addons_node_size(addons) + addons_root_size(addons));
  
  root->uv_loop = uv_loop;
  root->addons = addons;
  root->addon = NULL;
  
  node_init(&root->node, root, NULL, (node_addon_t*)((char*)root + sizeof(root_t)));
  
  root->addon = (root_addon_t*)((char*)root + sizeof(root_t) + addons_node_size(addons));
  
  int a;
  for (a = none; none != (a = addons_next(root->addons, a)); ) {
    const addon_vmt_t *vmt = addons_vmt(root->addons, a);
    if (vmt->root_init) vmt->root_init(root, a);
  }
  
  return root;
}

uv_loop_t *root_loop(const root_t *root) {
  return root->uv_loop;
}

root_addon_t *root_addon_ptr(const root_t *root, int key) {
  return root->addon ?
    (char*)root->addon + addons_root_offset(root->addons, key) :
    NULL;
}

int root_addon_key(const root_t *root, const root_addon_t *addon) {
  int key = none;
  size_t offset = (char*)addon - (char*)root->addon;
  
  for (; none != (key = addons_next(root->addons, key)); ) {
    if (offset == addons_root_offset(root->addons, key)) {
      return key;
    }
  }
  
  return none;
}

void root_del(root_t *root) {
  int a;
  for (a = none; none != (a = addons_next(root->addons, a)); ) {
    const addon_vmt_t *vmt = addons_vmt(root->addons, a);
    if (vmt->root_done) vmt->root_done(root, a);
  }
  
  node_done(&root->node);
  
  addons_del(root->addons);
  
  free(root);
}

int node_find(const node_t *parent, const char *name) {
  const khash_t(nodes) *h = &parent->children;
  khint_t i = kh_get(nodes, h, name);
  
  return i != kh_end(h) ? (int)i : none;
}

int node_pick(const node_t *parent, const node_t *child) {
  const khash_t(nodes) *h = &parent->children;
  khint_t i;
  
  for (i = kh_begin(h); i != kh_end(h); i++)
    if (kh_exist(h, i) && child == kh_val(h, i))
      return i;
  
  return none;
}

int node_next(const node_t *parent, int prev) {
  const khash_t(nodes) *h = &parent->children;
  khint_t i = prev == none ?
    kh_begin(h) : (khint_t)prev + 1;
  
  for (; ; i++) {
    if (i == kh_end(h))
      return none;
    if (kh_exist(h, i))
      break;
  }
  
  return (int)i;
}

const char *node_name(const node_t *parent, int key) {
  const khash_t(nodes) *h = &parent->children;
  khint_t i = key;
  
  assert(i < kh_end(h) && kh_exist(h, i));
  
  return kh_key(h, i);
}

node_t *node_node(const node_t *parent, int key) {
  const khash_t(nodes) *h = &parent->children;
  khint_t i = key;

  assert(i < kh_end(h) && kh_exist(h, i));
  
  return kh_val(h, i);
}

node_t *node_parent(const node_t *node) {
  return node->parent;
}

root_t *node_root(const node_t *node) {
  return node->root;
}

const char *node_id(node_t *node){
  node_t *parent = node_parent(node);
  
  if (!parent)
    return NULL;
  
  int key = node_pick(parent, node);
  
  assert(key != none);
  
  return node_name(parent, key);
}

node_addon_t *node_addon_ptr(const node_t *node, int key) {
  return ((char*)node->addon + addons_node_offset(node->root->addons, key));
}

int node_addon_key(const node_t *node, const node_addon_t *addon) {
  const addons_t *addons = node->root->addons;
  int key = none;
  size_t offset = (char*)addon - (char*)node->addon;
  
  for (; none != (key = addons_next(addons, key)); ) {
    if (offset == addons_node_offset(addons, key)) {
      return key;
    }
  }
  
  return none;
}

node_t* node_get(const node_t *node, const char *name) {
  const khash_t(nodes) *h = &node->children;
  khint_t i = kh_get(nodes, h, name);
  
  return i != kh_end(h) ?
    kh_val(h, i) : NULL;
}

bool node_del(node_t *node, const char *name) {
  khash_t(nodes) *h = &node->children;
  khint_t i = kh_get(nodes, h, name);
  
  if (i != kh_end(h)) {
    node_rem(kh_val(h, i));
    
    char *key = (char*)kh_key(h, i);
    
    kh_del(nodes, h, i);
    
    free(key);
    
    return true;
  }

  return false;
}

#define prop_a(k) (((k) >> 16) & 0xffff)
#define prop_p(k) ((k) & 0xffff)
#define prop_k(a, p) ((((a) & 0xffff) << 16) | ((p) & 0xffff))

int prop_next(node_t *node, int prev) {
  const addons_t *addons = node->root->addons;
  int a;
  int p;
  
  if (prev == none) {
    a = addons_next(addons, none);
    p = none;
  } else {
    a = (prev >> 16) & 0xffff;
    p = prev & 0xffff;
  }
  
  for (; ; ) {
    const addon_vmt_t *vmt = addons_vmt(addons, a);
    
    p = vmt->prop_next ? vmt->prop_next(node, a, p) : none;
    
    if (p == none) {
      a = addons_next(addons, a);
      
      if (a == none)
        return none;
    } else {
      break;
    }
  }
  
  return prop_k(a, p);
}

int prop_find(node_t *node, const char *name) {
  const addons_t *addons = node->root->addons;
  int a;
  int p;
  
  for (a = none; none != (a = addons_next(addons, a)); ) {
    const addon_vmt_t *vmt = addons_vmt(addons, a);

    if (vmt->prop_find) {
      if (none != (p = vmt->prop_find(node, a, name)))
        return prop_k(a, p);
    } else if (vmt->prop_next && vmt->prop_info) {
      for (p = none; none != (p = vmt->prop_next(node, a, p)); )
        if (0 == strcmp(name,
                        vmt->prop_info(node, a, p)->name))
          return prop_k(a, p);
    }
  }
  
  return none;
}

const prop_info_t *prop_info(node_t *node, int key) {
  const addons_t *addons = node->root->addons;
  int a = prop_a(key);
  int p = prop_p(key);
  const addon_vmt_t *vmt = addons_vmt(addons, a);
  
  return vmt->prop_info ? vmt->prop_info(node, a, p) : NULL;
}

char *prop_get(node_t *node, int key) {
  const addons_t *addons = node->root->addons;
  int a = prop_a(key);
  int p = prop_p(key);
  const addon_vmt_t *vmt = addons_vmt(addons, a);
  
  return vmt->prop_get ? vmt->prop_get(node, a, p) : NULL;
}

void prop_set(node_t *node, int key, const char *val) {
  const addons_t *addons = node->root->addons;
  int a = prop_a(key);
  int p = prop_p(key);
  const addon_vmt_t *vmt = addons_vmt(addons, a);
  
  vmt->prop_set ? vmt->prop_set(node, a, p, val) : NULL;
  
  for (a = none; none != (a = addons_next(addons, a)); ) {
    vmt = addons_vmt(addons, a);
    if (vmt->prop_trig)
      vmt->prop_trig(node, a, key);
  }
}
