#include "common.h"
#include "node.h"
#include "addon.h"

#include <getopt.h>
#include <stdio.h>

typedef struct {
  uv_loop_t loop;
  uv_signal_t sigint;
  root_t *root;
} daemon_t;

static void on_interrupt(uv_signal_t *signal, int signum) {
  (void)signum;
  
  daemon_t *daemon = signal->data;
  
  root_del(daemon->root);
  
  uv_signal_stop(&daemon->sigint);
}

void daemon_main(daemon_t *daemon) {
  uv_loop_init(&daemon->loop);
  daemon->loop.data = daemon;
  
  uv_signal_init(&daemon->loop, &daemon->sigint);
  daemon->sigint.data = daemon;
  
  daemon->root = root_new(&daemon->loop);
  
  uv_signal_start(&daemon->sigint, on_interrupt, SIGINT);
  
  if (uv_run(&daemon->loop, UV_RUN_DEFAULT)) {
    uv_run(&daemon->loop, UV_RUN_DEFAULT);
  }
  
  uv_loop_close(&daemon->loop);
}

typedef enum {
  show_help,
  show_version,
  show_addons,
  run_daemon,
} action_t;

int main(int argc, char* argv[]) {
  const char *short_opts = "dahvV";
  const struct option long_opts[] = {
    {"daemon",  no_argument, NULL, 'd'},
    {"addons",  no_argument, NULL, 'a'},
    {"verbose", no_argument, NULL, 'V'},
    {"help",    no_argument, NULL, 'h'},
    {"version", no_argument, NULL, 'v'},
    {NULL, 0, NULL, 0}
  };
  
  action_t f = show_help;
  bool v = false;
  int r;
  int i;
  
  for (; (r = getopt_long(argc, argv, short_opts, long_opts, &i)) != -1; ) {
		switch (r) {
    case 'd': f = run_daemon;   break;
    case 'a': f = show_addons;  break;
    case 'h': f = show_help;    break;
    case 'v': f = show_version; break;
    case 'V': v = 1; break;
    }
  }
  
  switch (f) {
  case run_daemon: {
    daemon_t daemon;
    daemon_main(&daemon);
  } break;
  case show_addons: {
    addons_t *addons = addons_new();
    
    for (i = none; none != (i = addons_next(addons, i)); ) {
      const addon_vmt_t *vmt = addons_vmt(addons, i);

      if (v) {
        const char *_true = "yes";
        const char *_false = "no";
#define impl(a) ((a) ? _true : _false)
        
        printf("- name: %s\n"
               "  info:\n"
               "    %s\n"
               "  root:\n"
               "    size: %zd\n"
               "    init: %s\n"
               "    done: %s\n"
               "  node:\n"
               "    size: %zd\n"
               "    init: %s\n"
               "    done: %s\n"
               "  prop:\n"
               "    next: %s\n"
               "    find: %s\n"
               "    info: %s\n"
               "    get:  %s\n"
               "    set:  %s\n",
               vmt->name,
               vmt->info,
               vmt->root_size,
               impl(vmt->root_init),
               impl(vmt->root_done),
               vmt->node_size,
               impl(vmt->node_init),
               impl(vmt->node_done),
               impl(vmt->prop_next),
               impl(vmt->prop_find),
               impl(vmt->prop_info),
               impl(vmt->prop_get),
               impl(vmt->prop_set));
#undef impl
      } else {
        printf("%s: %s\n", vmt->name, vmt->info);
      }
    }
    
    addons_del(addons);
  } break;
  case show_help: {
    printf("Usage: %s [options]\n"
           "Options:\n"
           "  -d|--daemon    Run daemon\n"
           "  -a|--addons    List addons\n"
           "  -v|--version   Show version and exit\n"
           "  -h|--help      Show this help\n"
           "  -V|--verbose   Verbose output\n"
           "Environment variables:\n"
           "  LINKO_PLUGIN_PATH  Colon separated paths to plugins\n"
           "  LINKO_STATE_DIR    The directory to save and restore state\n",
           argv[0]);
  } break;
  case show_version: {
    printf(LINKO_PROGRAM_STRING " " LINKO_VERSION_STRING "\n");
  } break;
  }
  
  return 0;
}
