typedef struct _root root_t;
typedef struct _node node_t;

/**
 * @brief The root addon object.
 */
typedef void root_addon_t;

/**
 * @brief The node addon object.
 */
typedef void node_addon_t;

root_t *root_new(uv_loop_t *uv_loop);
void root_del(root_t *root);
uv_loop_t *root_loop(const root_t *root);

#define root_node(root) ((node_t*)root)
root_addon_t *root_addon_ptr(const root_t *root, int key);
int root_addon_key(const root_t *root, const root_addon_t *addon);

int node_find(const node_t *parent, const char *name);
int node_pick(const node_t *parent, const node_t *child);
int node_next(const node_t *parent, int prev);
const char *node_name(const node_t *parent, int key);
node_t *node_node(const node_t *parent, int key);

node_t *node_parent(const node_t *node);
root_t *node_root(const node_t *node);
const char *node_id(node_t *node);
node_addon_t *node_addon_ptr(const node_t *node, int key);
int node_addon_key(const node_t *node, const node_addon_t *addon);

node_t* node_get(const node_t *node, const char *name);
node_t* node_add(node_t *node, const char *name);
bool node_del(node_t *node, const char *name);

typedef struct {
  const char *name;
  const char *type;
  const char *help;
} prop_info_t;

int prop_next(node_t *node, int prev);
int prop_find(node_t *node, const char *name);
const prop_info_t *prop_info(node_t *node, int key);
char *prop_get(node_t *node, int key);
void prop_set(node_t *node, int key, const char *val);
