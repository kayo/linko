#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <uv.h>

#ifndef LINKO_PLUGIN_PATH
#define LINKO_PLUGIN_PATH "/usr/lib/linko:./addon"
#endif

#ifndef LINKO_STATE_DIR
#define LINKO_STATE_DIR "."
#endif

#if UV_VERSION_MAJOR < 1
#error "libuv >= 1.0.0 required!"
#endif

#define __SYM_TO_STR(x) #x
#define _SYM_TO_STR(x) __SYM_TO_STR(x)
#define LINKO_PROGRAM_STRING _SYM_TO_STR(LINKO_PROGRAM)
#define LINKO_VERSION_STRING _SYM_TO_STR(LINKO_VERSION)

#define none (-1)

#define LOG_OFF    0
#define LOG_STDERR 1
#define LOG_WARN   2
#define LOG_SYSLOG 3

#ifndef WITH_LOG
#define WITH_LOG LOG_OFF
#endif

#if WITH_LOG == LOG_OFF
#define elog(fmt, ...)
#define wlog(fmt, ...)
#define ilog(fmt, ...)
#define dlog(fmt, ...)
#endif

#if WITH_LOG == LOG_STDERR
#include <stdio.h>
#define elog(fmt, ...) fprintf(stderr, "[ERROR]   " fmt "\n", ##__VA_ARGS__)
#define wlog(fmt, ...) fprintf(stderr, "[WARNING] " fmt "\n", ##__VA_ARGS__)
#define ilog(fmt, ...) fprintf(stderr, "[INFO]    " fmt "\n", ##__VA_ARGS__)
#define dlog(fmt, ...) fprintf(stderr, "[DEBUG]   " fmt "\n", ##__VA_ARGS__)
#endif

#if WITH_LOG == LOG_WARN
#include <err.h>
#define elog(fmt, ...) warn("[ERROR]   " fmt "\n", ##__VA_ARGS__)
#define wlog(fmt, ...) warn("[WARNING] " fmt "\n", ##__VA_ARGS__)
#define ilog(fmt, ...) warn("[INFO]    " fmt "\n", ##__VA_ARGS__)
#define dlog(fmt, ...) warn("[DEBUG]   " fmt "\n", ##__VA_ARGS__)
#endif

#if WITH_LOG == LOG_SYSLOG
#include <syslog.h>
#define elog(fmt, ...) syslog(LOG_ERR,     fmt "\n", ##__VA_ARGS__)
#define wlog(fmt, ...) syslog(LOG_WARNING, fmt "\n", ##__VA_ARGS__)
#define ilog(fmt, ...) syslog(LOG_INFO,    fmt "\n", ##__VA_ARGS__)
#define dlog(fmt, ...) syslog(LOG_DEBUG,   fmt "\n", ##__VA_ARGS__)
#endif

#define safe_uv(action, ...) {                    \
    int _uv_err = (action);                       \
    if(_uv_err < 0 && _uv_err != UV_EOF) {        \
      elog("%s: %s",                              \
          uv_err_name(_uv_err),                   \
          uv_strerror(_uv_err));                  \
      __VA_ARGS__;                                \
      exit(_uv_err);                              \
    }                                             \
  }
