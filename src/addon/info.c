#include "common.h"
#include "node.h"
#include "addon.h"

static const prop_info_t info_prop_list[] = {
  {"title", "string", "The title of a node"},
  {"description", "string", "The node description"},
};

#define info_num_props (sizeof(info_prop_list)/sizeof(info_prop_list[0]))

typedef struct {
  char *info_field[info_num_props];
  char *end_fields[0];
} info_t;

static void info_node_init(node_t *node, int addon) {
  (void)node;
  info_t *info = node_addon_ptr(node, addon);
  
  char **field = info->info_field;

  for (; field < info->end_fields; field++) *field = NULL;
}

static void info_node_done(node_t *node, int addon) {
  (void)node;
  info_t *info = node_addon_ptr(node, addon);
  
  char **field = info->info_field;
  
  for (; field < info->end_fields; field++) if (*field) free(*field);
}

static int info_prop_next(node_t *node, int addon, int prop) {
  (void)node;
  (void)addon;
  
  return prop == none ?
    0 :
    prop < (int)info_num_props - 1 ?
    prop + 1 :
    none;
}

static int info_prop_find(node_t *node, int addon, const char *name) {
  (void)node;
  (void)addon;

  int i;

  for (i = 0; i < (int)info_num_props; i++) {
    if (0 == strcmp(name, info_prop_list[i].name))
      return i;
  }
  
  return none;
}

static const prop_info_t *info_prop_info(node_t *node, int addon, int prop) {
  (void)node;
  (void)addon;
  
  return info_prop_list + prop;
}

static char *info_prop_get(node_t *node, int addon, int prop) {
  (void)node;
  info_t *info = node_addon_ptr(node, addon);
  
  const char *val = info->info_field[prop];
  return val ? strdup(val) : NULL;
}

static void info_prop_set(node_t *node, int addon, int prop, const char *val) {
  (void)node;
  info_t *info = node_addon_ptr(node, addon);
  
  char **field = info->info_field + prop;
  
  if (*field) free(*field);

  if (val) *field = strdup(val);
  else *field = NULL;
}

addon_vmt_t addon_symbol(info) = {
  "info",
  "Adds some useful human-readable properties to nodes, like title and description.",
  
  0,
  
  NULL,
  NULL,
  
  sizeof(info_t),
  
  info_node_init,
  info_node_done,
  
  info_prop_next,
  info_prop_find,
  info_prop_info,
  info_prop_get,
  info_prop_set,
  NULL,
};
