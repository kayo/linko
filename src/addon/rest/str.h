/* IMPORTANT NOTES:
 * The dst string must be at least in two (2) times longer than src.
 * The dst pointer must not to be the same as src.
 * Function returns the end of dst.
 */
char *strescape(char *dst, const char *src);

int kputs_strescape(const char *p, kstring_t *s);

/* IMPORTANT NOTES:
 * The dst pointer may be the same as src but src may be changed in this case.
 * Function returns the end of dst.
 */
char *strunescape(char *dst, const char *src);
