#include <kstring.h>

static inline int ishex(char c) {
  return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
}

static inline int isurl(char c) {
  return ('0' <= c && c <= '9') || ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || c == '-' || c == '_' || c == '.' || c == '~';
}

static inline char hex2i(char c) {
  return c - (c <= '9' ? '0' : (c <= 'Z' ? 'A' : 'a') - 10);
}

static const char hex[] = "0123456789abcdef";

static inline char i2hex(char h) {
  return hex[h & 0xff];
}

char *urlencode(char *dst, const char *src) {
  for (; *src; src++) {
    if (isurl(*src))
      *dst++ = *src;
    else if (*src == ' ')
      *dst++ = '+';
    else {
      *dst++ = '%';
      *dst++ = i2hex(*src >> 4);
      *dst++ = i2hex(*src & 0xff);
    }
  }
  *dst = '\0';
  return dst;
}

int kputs_urlencode(const char *p, kstring_t *s) {
  size_t l = ks_len(s);
  ks_resize(s, l + (strlen(p) << 2));
  char *t = ks_str(s) + l;
  l += urlencode(t, p) - t;
  ks_len(s) = l;
  ks_resize(s, l);
  return l;
}

char *urldecode(char *dst, const char *src) {
  for (; *src; src++) {
    switch (*src) {
    case '%':
      if (ishex(src[1]) && ishex(src[2])) {
        *dst++ = (hex2i(src[1]) << 4) | hex2i(src[2]);
        src += 2;
      }
      break;
    case '+':
      *dst++ = ' ';
      break;
    default:
      *dst++ = *src;
    }
  }
  *dst = '\0';
  return dst;
}
