#include <kstring.h>

char *strescape(char *dst, const char *src) {
  for (; *src; src++) {
    switch (*src) {
    case '"':
      *dst++ = '\\';
      *dst++ = '"';
      break;
    case '\\':
      *dst++ = '\\';
      *dst++ = '\\';
      break;
    case '\n':
      *dst++ = '\\';
      *dst++ = 'n';
      break;
    case '\r':
      *dst++ = '\\';
      *dst++ = 'r';
      break;
    case '\t':
      *dst++ = '\\';
      *dst++ = 't';
      break;
    case '\v':
      *dst++ = '\\';
      *dst++ = 'v';
      break;
    case '\f':
      *dst++ = '\\';
      *dst++ = 'f';
      break;
    case '\a':
      *dst++ = '\\';
      *dst++ = 'a';
      break;
    case '\b':
      *dst++ = '\\';
      *dst++ = 'b';
      break;
    default:
      *dst++ = *src;
      break;
    }
  }
  *dst = '\0';
  return dst;
}

int kputs_strescape(const char *p, kstring_t *s) {
  size_t l = ks_len(s);
  ks_resize(s, l + (strlen(p) << 1));
  char *t = ks_str(s) + l;
  l += strescape(t, p) - t;
  ks_len(s) = l;
  ks_resize(s, l);
  return l;
}

static inline int ishex(char c) {
  return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
}

static inline char hex2i(char c) {
  return c - (c <= '9' ? '0' : (c <= 'Z' ? 'A' : 'a') - 10);
}

char *strunescape(char *dst, const char *src) {
  for (; *src; src++) {
    if (*src == '\\') {
      src++;
      switch (*src) {
      case 'n':
        *dst++ = '\n';
        break;
      case 'r':
        *dst++ = '\r';
        break;
      case 't':
        *dst++ = '\t';
        break;
      case 'v':
        *dst++ = '\v';
        break;
      case 'f':
        *dst++ = '\f';
        break;
      case 'a':
        *dst++ = '\a';
        break;
      case 'b':
        *dst++ = '\b';
        break;
      case 'x':
        if (ishex(src[1]) && ishex(src[2])) {
          *dst++ = (hex2i(src[1]) << 4) | hex2i(src[2]);
          src += 2;
        }
      case 'u':
        if (ishex(src[1]) && ishex(src[2]) &&
            ishex(src[3]) && ishex(src[4])) {
          *dst++ = (hex2i(src[1]) << 4) | hex2i(src[2]);
          *dst++ = (hex2i(src[3]) << 4) | hex2i(src[4]);
          src += 4;
        }
      default:
        *dst++ = *src;
        break;
      }
    } else {
      *dst++ = *src;
    }
  }
  *dst = '\0';
  return dst;
}
