/* IMPORTANT NOTES:
 * The dst string must be at least in three (3) times longer than src.
 * The dst pointer must not to be the same as src.
 * Function returns the end of dst.
 */
char *urlencode(char *dst, const char *src);

int kputs_urlencode(const char *p, kstring_t *s);

/* IMPORTANT NOTES:
 * The dst pointer may be the same as src but src may be changed in this case.
 * Function returns the end of dst.
 */
char *urldecode(char *dst, const char *src);
