#ifndef __JSON_PARSER_H__
#define __JSON_PARSER_H__ "json_parser.h"

/**
 * @brief Simple and fast JSON tokenizer
 *
 * Streamed push-style JSON tokenizer.
 */

/**
 * @brief Tokenizer type
 */
typedef struct _json_tokenizer json_tokenizer;

/**
 * @brief Tokenizer settings type
 */
typedef struct _json_tokenizer_settings json_tokenizer_settings;

/**
 * @brief The token callback
 *
 * @param t The tokenizer state object
 * @return On success this callback must return 1. The 0 means processing error.
 */
typedef int json_tokenizer_cb(json_tokenizer *t);

/**
 * @brief The data-token callback
 *
 * @param t The tokenizer state object
 * @param ptr The pointer to beginning of data chunk
 * @param len The length of data chunk
 * @return On success this callback must return 1. The 0 means processing error.
 */
typedef int json_tokenizer_data_cb(json_tokenizer *t, const char *ptr, size_t len);

/**
 * @brief Tokenizer settings structure
 */
struct _json_tokenizer_settings {
  /**
   * @brief The object open token callback
   */
  json_tokenizer_cb *on_object_open;
  /**
   * @brief The object close token callback
   */
  json_tokenizer_cb *on_object_close;

  /**
   * @brief The array open token callback
   */
  json_tokenizer_cb *on_array_open;
  /**
   * @brief The array close token callback
   */
  json_tokenizer_cb *on_array_close;

  /**
   * @brief The key-value separator callback
   */
  json_tokenizer_cb *on_kv_separate;
  /**
   * @brief The element separator callback
   */
  json_tokenizer_cb *on_el_separate;

  /**
   * @brief The null token chunk callback
   *
   * This callback will be called more than once in case of partial data.
   */
  json_tokenizer_data_cb *on_null_chunk;
  /**
   * @brief The boolean token chunk callback
   *
   * This callback will be called more than once in case of partial data.
   */
  json_tokenizer_data_cb *on_bool_chunk;
  
  /**
   * @brief The number token chunk callback
   *
   * This callback will be called more than once in case of partial data.
   */
  json_tokenizer_data_cb *on_number_chunk;
  /**
   * @brief The string token chunk callback
   *
   * The chunk is string data without beginning and end double quotes.
   * This callback will be called more than once in case of partial data.
   */
  json_tokenizer_data_cb *on_string_chunk;
};

/**
 * @brief Tokenizer structure
 */
struct _json_tokenizer {
  /**
   * @brief The current state of tokenizer
   */
  void *tokenize;
  
  /**
   * @brief The current atomic value callback
   */
  void *atom;
  
  /**
   * @brief The pointer to user-defined data
   */
  void *data;
};

/**
 * @brief The initializer function
 *
 * @param t The tokenizer state object
 * @param d The pointer to userdata
 */
void json_tokenizer_init(json_tokenizer *t);

#define json_tokenizer_success (-2)
#define json_tokenizer_failed(x) (x > -1)

/**
 * @brief The tokenizer function
 *
 * @param t The tokenizer state object
 * @param s The tokenizer settings
 * @param ptr The pointer to beginning of source data
 * @param len The length of source data to tokenize
 * @return Function returns 0 on success. The value > 0 means position of invalid token.
 *
 * Failed tokenizer must be re-initialized before next executing.
 */
int json_tokenizer_execute(json_tokenizer *t, const json_tokenizer_settings *s, const char *ptr, size_t len);

/*
#include <string.h>

static inline int json_tokenizer_execute_string(json_tokenizer *t, const json_tokenizer_settings *s, const char *str) {
  return json_tokenizer_execute(t, s, str, strlen(str));
}
*/

#endif/*__JSON_PARSER_H__*/
