#include "common.h"
#include "node.h"
#include "addon.h"

#include <kstring.h>
#include <sqlite3.h>

#define DATA_DEF_QUERY                                                  \
  "CREATE TABLE IF NOT EXISTS node (id INTEGER PRIMARY KEY, name VARCHAR NOT NULL, parent INTEGER KEY NOT NULL);" \
  "CREATE UNIQUE INDEX IF NOT EXISTS node_uniq ON node(name, parent);"  \
  "CREATE TABLE IF NOT EXISTS prop (node INTEGER KEY NOT NULL, name VARCHAR KEY NOT NULL, value TEXT NOT NULL);" \
  "CREATE UNIQUE INDEX IF NOT EXISTS prop_uniq ON prop(node, name);"

#define NODE_GET_QUERY "SELECT id, name FROM node WHERE parent = ?"
#define NODE_ADD_QUERY "INSERT INTO node (name, parent) VALUES(?, ?)"
#define NODE_DEL_QUERY "DELETE FROM node WHERE id = ?"

#define PROP_GET_QUERY "SELECT name, value FROM prop WHERE node = ?"
#define PROP_SET_QUERY "INSERT OR REPLACE INTO prop(node, name, value) VALUES(?, ?, ?)"
#define PROP_DEL_QUERY "DELETE FROM prop WHERE node = ? AND name = ?"

typedef struct {
  sqlite3 *db;
  sqlite3_stmt *node_add;
  sqlite3_stmt *node_del;
  sqlite3_stmt *prop_set;
  sqlite3_stmt *prop_del;
  bool run;
} save_root_t;

typedef struct {
  int id;
} save_node_t;

static void save_node_load(sqlite3_stmt *node_get, node_t *parent, int addon) {
  save_node_t *parent_addon = node_addon_ptr(parent, addon);
  
  sqlite3_bind_int(node_get, 1, parent_addon->id);
  
  for (; ;)
    switch (sqlite3_step(node_get)) {
    case SQLITE_ROW: {
      const char *name = (const char*)sqlite3_column_text(node_get, 1);
      node_t *node = node_add(parent, name);
      
      if (!node)
        elog("Unable to restore node `%s`", name);
      
      save_node_t *node_addon = node_addon_ptr(node, addon);
      node_addon->id = sqlite3_column_int(node_get, 0);
      
      dlog("Node `%s` loaded as #%d", name, node_addon->id);
    } break;
    case SQLITE_DONE: {
      sqlite3_reset(node_get);
      
      int key;
      
      for (key = none; none != (key = node_next(parent, key)); )
        save_node_load(node_get, node_node(parent, key), addon);
      
      return;
    } break;
    }
}

static void save_prop_load(sqlite3_stmt *prop_get, node_t *node, int addon) {
  save_node_t *node_addon = node_addon_ptr(node, addon);
  
  sqlite3_bind_int(prop_get, 1, node_addon->id);
  
  dlog("Load props of node `%s`", node_id(node));
  
  for (; ;)
    switch (sqlite3_step(prop_get)) {
    case SQLITE_ROW: {
      const char *name = (const char*)sqlite3_column_text(prop_get, 0);
      int key = prop_find(node, name);
      
      if (key != none) {
        const char *val = (const char*)sqlite3_column_text(prop_get, 1);
        prop_set(node, key, val);
        dlog("Prop `%s` set value `%s`.", name, val);
      }
    } break;
    case SQLITE_DONE: {
      sqlite3_reset(prop_get);
      
      int key;
      
      for (key = none; none != (key = node_next(node, key)); )
        save_prop_load(prop_get, node_node(node, key), addon);
      
      return;
    } break;
    }
}

static void save_stmt_done(sqlite3_stmt **stmt) {
  for (; *stmt; stmt++)
    sqlite3_finalize(*stmt);
}

static void save_root_init(root_t *root, int addon) {
  (void)root;
  save_root_t *root_addon = root_addon_ptr(root, addon);
  save_node_t *node_addon = node_addon_ptr(root_node(root), addon);
  
  root_addon->run = false;
  node_addon->id = 0;
  
  const char *state_dir = getenv("LINKO_STATE_DIR");
  
  if (!state_dir)
    state_dir = LINKO_STATE_DIR;
  
  kstring_t path = ks_null();
  
  kputs(state_dir, &path);
  kputc('/', &path);
  kputs("root.db", &path);
  
  switch (sqlite3_open_v2(ks_str(&path), &root_addon->db,
                          SQLITE_OPEN_NOMUTEX |
                          SQLITE_OPEN_READWRITE |
                          SQLITE_OPEN_CREATE, "unix-none")) {
  case 0:
    ilog("Database `%s` opened", ks_str(&path));
    break;
  default:
    elog("Can't open database `%s`: %s\n", ks_str(&path),
         sqlite3_errmsg(root_addon->db));
    sqlite3_close(root_addon->db);
    return;
  }
  
  free(ks_str(&path));
  
  char *msg;
  
  switch (sqlite3_exec(root_addon->db, DATA_DEF_QUERY, NULL, 0, &msg)) {
  case SQLITE_OK:
    break;
  default:
    elog("SQL error: %s\n", msg);
    sqlite3_free(msg);
    return;
  }
  
  /* Parepare queries */
  const char *names[] = {
    "node get",
    "node add",
    "node del",
    "prop get",
    "prop set",
  };
  
  const char *queries[] = {
    NODE_GET_QUERY,
    NODE_ADD_QUERY,
    NODE_DEL_QUERY,
    PROP_GET_QUERY,
    PROP_SET_QUERY,
  };

  sqlite3_stmt *node_get;
  sqlite3_stmt *prop_get;
  
  sqlite3_stmt **stmts[] = {
    &node_get,
    &root_addon->node_add,
    &root_addon->node_del,
    &prop_get,
    &root_addon->prop_set,
  };
  
  int i;
  
  for (i = 0; i < 5; i++)
    switch (sqlite3_prepare_v2(root_addon->db, queries[i], -1, stmts[i], 0)) {
    case SQLITE_OK:
      dlog("Query `%s` prepared", names[i]);
      break;
    default:
      elog("Unable to prepare `%s` query: %s", names[i],
           sqlite3_errmsg(root_addon->db));
      sqlite3_close(root_addon->db);
      return;
    }
  
  /* Restore nodes from database */
  save_node_load(node_get, root_node(root), addon);

  /* Restore props from database */
  save_prop_load(prop_get, root_node(root), addon);

  /* Finalize prepared queries */
  sqlite3_stmt *stmts_[] = {
    node_get,
    prop_get,
    NULL,
  };
  
  save_stmt_done(stmts_);
  
  root_addon->run = true;
}

static void save_root_done(root_t *root, int addon) {
  (void)root;
  save_root_t *root_addon = root_addon_ptr(root, addon);
  
  root_addon->run = false;
  
  /* Finalize prepared queries */
  sqlite3_stmt *stmts_[] = {
    root_addon->node_add,
    root_addon->node_del,
    root_addon->prop_set,
    root_addon->prop_del,
    NULL,
  };
  
  save_stmt_done(stmts_);
  
  sqlite3_close_v2(root_addon->db);
  
  ilog("Database closed");
}

static void save_node_init(node_t *node, int addon) {
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);
  
  if (!parent || !root_addon->run)
    return;
  
  save_node_t *node_addon = node_addon_ptr(node, addon);  
  int key = node_pick(parent, node);
  const char *name = node_name(parent, key);
  save_node_t *node_parent_addon = node_addon_ptr(parent, addon);
  
  sqlite3_bind_text(root_addon->node_add, 1, name, -1, SQLITE_STATIC);
  sqlite3_bind_int(root_addon->node_add, 2, node_parent_addon->id);

  switch (sqlite3_step(root_addon->node_add)) {
  case SQLITE_DONE:
    node_addon->id = sqlite3_last_insert_rowid(root_addon->db);
    
    dlog("Node `%s` inserted as #%d.", name, node_addon->id);
    break;
  default:
    elog("Insert node `%s` failed.", name);
  }
  
  sqlite3_reset(root_addon->node_add);
}

static void save_node_done(node_t *node, int addon) {
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);

  if (!parent || !root_addon->run)
    return;
  
  save_node_t *node_addon = node_addon_ptr(node, addon);
  int key = node_pick(parent, node);
  const char *name = node_name(parent, key);
  
  sqlite3_bind_int(root_addon->node_del, 1, node_addon->id);
  
  switch (sqlite3_step(root_addon->node_del)) {
  case SQLITE_DONE:
    dlog("Node `%s` deleted as #%d.", name, node_addon->id);
    break;
  default:
    elog("Delete node `%s` failed.", name);
  }
  
  sqlite3_reset(root_addon->node_del);
}

static void save_prop_trig(node_t *node, int addon, int key) {
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);
  
  if (!parent || !root_addon->run)
    return;
  
  save_node_t *node_addon = node_addon_ptr(node, addon);
  const prop_info_t *info = prop_info(node, key);
  char *val = prop_get(node, key);

  sqlite3_stmt *stmt = val ? root_addon->prop_set : root_addon->prop_del;
  
  sqlite3_bind_int(stmt, 1, node_addon->id);
  sqlite3_bind_text(stmt, 2, info->name, -1, SQLITE_STATIC);
  sqlite3_bind_text(stmt, 3, val, -1, free);

  switch (sqlite3_step(stmt)) {
  case SQLITE_DONE:
    dlog("Prop `%s` of node `%s` saved.", info->name, node_id(node));
    break;
  default:
    elog("Saving prop `%s` of node `%s` failed.", info->name, node_id(node));
  }
  
  sqlite3_reset(stmt);
}

addon_vmt_t addon_symbol(save) = {
  "save",
  "Saves structure and properties changes in sqlite3 database to restore it at the next time.",
  
  sizeof(save_root_t),
  
  save_root_init,
  save_root_done,
  
  sizeof(save_node_t),
  
  save_node_init,
  save_node_done,
  
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  save_prop_trig,
};
