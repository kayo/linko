#include "common.h"
#include "node.h"
#include "addon.h"

#include <kstring.h>

#include <sys/types.h>
#include <unistd.h>
#include <dirent.h>

#define NODE_NAME_EXT ".node"
#define PROP_NAME_EXT ".prop"

typedef struct {
  const char *path;
  bool run;
} save_root_t;

static void save_node_load(kstring_t *path, node_t *parent) {
  DIR *dir = opendir(ks_str(path));

  if (!dir) {
    elog("Unable to open dir `%s`", ks_str(path));
    return;
  }
  
  struct dirent *ent;

  kputc('/', path);
  size_t pfx_len = ks_len(path);
  
  for (; NULL != (ent = readdir(dir)); ) {
    char *ext = strrchr(ent->d_name, '.');
    
    if (!ext || 0 != strcmp(ext, NODE_NAME_EXT))
      continue;

    *ext = '\0';
    const char *name = ent->d_name;
    
    node_t *node = node_add(parent, name);
    
    if (node) {
      dlog("Node `%s` loaded", name);
    } else {
      elog("Unable to restore node `%s`", name);
    }
  }
  
  closedir(dir);
  
  int key;
  
  for (key = none; none != (key = node_next(parent, key)); ) {
    kputs(node_name(parent, key), path);
    kputsc(NODE_NAME_EXT, path);
    
    save_node_load(path, node_node(parent, key));
    
    ks_cut(path, pfx_len);
  }
}

static char *save_prop_read(const char *path) {
  int fd = open(path, O_RDONLY);
  
  if (fd < 0) {
    elog("Unable to open file `%s` for read.", path);
    return NULL;
  }
  
  off_t len = lseek(fd, 0, SEEK_END);

  if (len < 0) {
    elog("Unable to seek file `%s`.", path);
    close(fd);
    return NULL;
  }
  
  lseek(fd, 0, SEEK_SET);
  
  char *val = malloc(len + 1);
  char *ptr = val;
  size_t cnt = len;
  ssize_t rs;
  
  for (; 0 != (rs = read(fd, ptr, cnt)); ) {
    if (rs < 0) {
      elog("Error when reading file `%s`", path);
      free(val);
      close(fd);
      return NULL;
    }
    
    ptr += rs;
    cnt -= rs;
  }
  
  close(fd);
  
  *ptr = '\0';
  
  return val;
}

static void save_prop_load(kstring_t *path, node_t *node) {
  DIR *dir = opendir(ks_str(path));
  
  if (!dir) {
    elog("Unable to open dir `%s`", ks_str(path));
    return;
  }
  
  struct dirent *ent;
  
  kputc('/', path);
  size_t pfx_len = ks_len(path);
  
  dlog("Load props of node `%s`", node_id(node));
  
  for (; NULL != (ent = readdir(dir)); ) {
    char *ext = strrchr(ent->d_name, '.');
    
    if (!ext || 0 != strcmp(ext, PROP_NAME_EXT))
      continue;
    
    *ext = '\0';
    const char *name = ent->d_name;
    
    int key = prop_find(node, name);
    
    if (key != none) {
      *ext = '.';
      kputs(ent->d_name, path);
      *ext = '\0';
      
      char *val = save_prop_read(ks_str(path));
      
      if (val) {
        prop_set(node, key, val);
        dlog("Prop `%s` set value `%s`.", name, val);
        free(val);
      }
      
      ks_cut(path, pfx_len);
    }
  }
  
  closedir(dir);
  
  int key;
  
  for (key = none; none != (key = node_next(node, key)); ) {
    kputs(node_name(node, key), path);
    kputsc(NODE_NAME_EXT, path);
    
    save_prop_load(path, node_node(node, key));
    
    ks_cut(path, pfx_len);
  }
}

static void save_root_path(kstring_t *path){
  static const char *state_dir = NULL;
  
  if (!state_dir) {
    state_dir = getenv("LINKO_STATE_DIR");
    
    if (!state_dir)
      state_dir = LINKO_STATE_DIR;
  }
  
  kputs(state_dir, path);
  kputc('/', path);
  kputs("root" NODE_NAME_EXT, path);
}

static void save_root_init(root_t *root, int addon) {
  (void)root;
  save_root_t *root_addon = root_addon_ptr(root, addon);
  
  root_addon->run = false;
  
  kstring_t path = ks_null();
  save_root_path(&path);
  
  /* Create root directory */
  mkdir(ks_str(&path), 0777);
  
  /* Restore nodes from database */
  save_node_load(&path, root_node(root));
  
  /* Restore props from database */
  save_prop_load(&path, root_node(root));
  
  free(ks_str(&path));
  
  root_addon->run = true;
}

static void save_root_done(root_t *root, int addon) {
  (void)root;
  save_root_t *root_addon = root_addon_ptr(root, addon);
  
  root_addon->run = false;
}

static void save_node_path(kstring_t *path, node_t *node) {
  node_t *parent = node_parent(node);
  
  if (parent) {
    save_node_path(path, parent);
    kputc('/', path);
    kputs(node_id(node), path);
    kputsc(NODE_NAME_EXT, path);
  } else {
    save_root_path(path);
  }
}

static void save_node_init(node_t *node, int addon) {
  (void)addon;
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);
  
  if (!parent || !root_addon->run)
    return;
  
  kstring_t path = ks_null();
  
  save_node_path(&path, node);
  
  switch (mkdir(ks_str(&path), 0777)) {
  case 0:
    dlog("Node `%s` created.", node_id(node));
    break;
  default:
    elog("Creating node `%s` failed.", node_id(node));
  }
  
  free(ks_str(&path));
}

static void save_node_done(node_t *node, int addon) {
  (void)addon;
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);
  
  if (!parent || !root_addon->run)
    return;
  
  kstring_t path = ks_null();
  
  save_node_path(&path, node);
  
  switch (rmdir(ks_str(&path))) {
  case 0:
    dlog("Node `%s` deleted.", node_id(node));
    break;
  default:
    elog("Delete node `%s` failed.", node_id(node));
  }
  
  free(ks_str(&path));
}

static void save_prop_path(kstring_t *path, node_t *node, const char *name) {
  save_node_path(path, node);
  kputc('/', path);
  kputs(name, path);
  kputsc(PROP_NAME_EXT, path);
}

static bool save_prop_dump(const char *path, const char *val) {
  int fd = open(path, O_WRONLY | O_CREAT);
  
  if (fd < 0) {
    elog("Unable to open file `%s` for write.", path);
    return false;
  }
  
  const char *ptr = val;
  size_t cnt = strlen(val);
  ssize_t ws;
  
  for (; cnt > 0; ) {
    ws = write(fd, ptr, cnt);
    
    if (ws < 0) {
      elog("Error when writing file `%s`", path);
      close(fd);
      return false;
    }
    
    ptr += ws;
    cnt -= ws;
  }
  
  close(fd);
  
  return true;
}

static void save_prop_trig(node_t *node, int addon, int key) {
  save_root_t *root_addon = root_addon_ptr(node_root(node), addon);
  node_t *parent = node_parent(node);
  
  if (!parent || !root_addon->run)
    return;
  
  const prop_info_t *info = prop_info(node, key);
  char *val = prop_get(node, key);
  kstring_t path = ks_null();
  
  save_prop_path(&path, node, info->name);
  
  switch (save_prop_dump(ks_str(&path), val)) {
  case true:
    dlog("Prop `%s` of node `%s` saved.", info->name, node_id(node));
    break;
  default:
    elog("Saving prop `%s` of node `%s` failed.", info->name, node_id(node));
  }
  
  free(val);
  free(ks_str(&path));
}

addon_vmt_t addon_symbol(save) = {
  "save",
  "Saves structure and properties changes in file system to restore it at the next time.",
  
  sizeof(save_root_t),
  
  save_root_init,
  save_root_done,
  
  0,
  
  save_node_init,
  save_node_done,
  
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  save_prop_trig,
};
