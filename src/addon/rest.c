#include "common.h"
#include "node.h"
#include "addon.h"

#include <time.h>

#include <klist.h>
#include <kstring.h>

#include "rest/url.h"
#include "rest/str.h"
#include "rest/json_parser.h"

#include <http_parser.h>

typedef const char *str_key_t;
typedef struct {
  const char *key;
  const char *val;
} str_key_val_t;

typedef enum {
  HEADER_NONE = 0,
  HEADER_ACCEPT = 1,
  HEADER_CONTENT_TYPE = 2,
} last_header_t;

typedef enum {
  MIME_NONE = 0,
  MIME_JSON = 1,
  MIME_YAML = 2,
  MIME_HTML = 3,
} mime_type_t;

static const char *content_type[] = {
  "text/plain, charset=utf8",
  "application/json",
  "application/x-yaml",
  "text/html, charset=utf8"
};

typedef struct http_service http_service_t;

__KLIST_TYPE(uv_buf_t, uv_buf_t)

typedef struct {
  http_service_t *service;
  uv_tcp_t uv_tcp;
  http_parser http_parser;
  json_tokenizer json_tokenizer;
  
  kstring_t buffer;
  bool header_field: 1;
  last_header_t last_header: 2;
  mime_type_t accept_type: 2;
  mime_type_t content_type: 2;
  unsigned int id: 24;

  bool in_object: 1;
  bool prop_name: 1;
  node_t *node;
  int prop;
  
  uv_write_t uv_write;
  bool is_write;
  klist_t(uv_buf_t) write_queue;
} http_connection_t;

#define free_buf(cur) free((cur)->data.base)
__KLIST_IMPL(uv_buf_t, static inline klib_unused, uv_buf_t, free_buf)

#define con_free(x)
KMEMPOOL_INIT(conpool, http_connection_t, con_free);

#ifndef HTTP_BUFFER_SIZE
#define HTTP_BUFFER_SIZE 128
#endif

typedef char buf_t[HTTP_BUFFER_SIZE];
#define buf_free(x)
KMEMPOOL_INIT(bufpool, buf_t, buf_free);

struct http_service {
  root_t *root;
  uv_tcp_t uv_tcp;
  http_parser_settings http_parser_settings;
  json_tokenizer_settings json_tokenizer_settings;
  struct sockaddr_in bind_addr;

  kmempool_t(conpool) con_pool;
  kmempool_t(bufpool) buf_pool;
  int connections: 30;
  bool running: 1;
  bool stopped: 1;
};

static void on_stop(uv_handle_t* handle) {
  (void)handle;
  http_service_t *service = handle->data;
  
  service->stopped = true;
  
  kmp_finalize(conpool, &service->con_pool);
  kmp_finalize(bufpool, &service->buf_pool);
  
  dlog("rest stop");
}

static void on_close(uv_handle_t* handle) {
  http_connection_t* conn = handle->data;
  http_service_t *service = conn->service;
  
  char *ptr = ks_release(&conn->buffer);
  if (ptr) free(ptr);
  
  dlog("conn %u close", conn->id);
  
  kl_finalize(uv_buf_t, &conn->write_queue);
  
  service->connections --;

  if (!service->running && !service->connections)
    uv_close((uv_handle_t*)&service->uv_tcp, on_stop);
  
  kmp_free(conpool, &service->con_pool, conn);
}

static void on_alloc(uv_handle_t* handle, size_t size, uv_buf_t *buf) {
  (void)size; /* ignore size, we used mempool */
  
  http_connection_t *conn = handle->data;
  http_service_t *service = conn->service;
  
  buf->base = (char*)kmp_alloc(bufpool, &service->buf_pool);
  buf->len = sizeof(buf_t);
}

static void on_read(uv_stream_t* handle, ssize_t nread, const uv_buf_t *buf) {
  http_connection_t *conn = handle->data;
  http_service_t *service = conn->service;
  
  dlog("conn %u read %zd", conn->id, nread);
  
  if (nread < 0) { /* end of file */
    uv_close((uv_handle_t*)&conn->uv_tcp, on_close);
  } else {
    int parsed = http_parser_execute(&conn->http_parser,
                                     &service->http_parser_settings,
                                     buf->base, nread);
    
    if (parsed < nread) { /* parse error */
      uv_close((uv_handle_t*)&conn->uv_tcp, on_close);
    }
  }
  
  kmp_free(bufpool, &service->buf_pool, (buf_t*)buf->base);
}

static void on_connect(uv_stream_t* handle, int status) {
  http_service_t *service = handle->data;
  
  if (!service->running || status < 0)
    return;
  
  http_connection_t *conn = kmp_alloc(conpool, &service->con_pool);
  
  conn->id = service->connections;
  
  dlog("conn %u connect", conn->id);
  
  conn->service = service;
  
  uv_tcp_init(root_loop(service->root), &conn->uv_tcp);
  
  http_parser_init(&conn->http_parser, HTTP_REQUEST);
  
  ks_release(&conn->buffer);
  
  conn->header_field = false;
  conn->last_header = HEADER_NONE;
  conn->accept_type = MIME_HTML;
  conn->content_type = MIME_NONE;
  conn->node = NULL;
  
  conn->is_write = false;
  kl_prepare(uv_buf_t, &conn->write_queue);
  
  conn->http_parser.data =
    conn->uv_tcp.data =
    conn->uv_write.data =
    conn;
  
  if (uv_accept(handle, (uv_stream_t*)&conn->uv_tcp)) {
    uv_close((uv_handle_t*)&conn->uv_tcp, NULL);
  }

  service->connections ++;
  
  uv_read_start((uv_stream_t*)&conn->uv_tcp, on_alloc, on_read);
}

static void do_write(http_connection_t* conn);

static void on_write(uv_write_t* req, int status) {
  http_connection_t *conn = req->data;
  
  //free(c->uv_write_buf.base);

  dlog("conn %u write %d", conn->id, status);
  
  kl_shift(uv_buf_t, &conn->write_queue, NULL);
  
  safe_uv(status);
  
  if (0 == status) {
    conn->is_write = false;
    
    do_write(conn);
  }
}

static void do_write(http_connection_t* conn) {
  kliter_t(uv_buf_t) *cur = kl_begin(&conn->write_queue);
  
  if (cur != kl_end(&conn->write_queue)) {
    if (!conn->is_write) {
      conn->is_write = true;
      
      uv_write(&conn->uv_write,
               (uv_stream_t*)&conn->uv_tcp,
               &kl_val(cur), 1,
               on_write);
    }
  } else {
    uv_close((uv_handle_t*)&conn->uv_tcp, on_close);
  }
}

static void do_respond(http_connection_t *conn,
                       unsigned status,
                       const char *message,
                       const char *mimetype,
                       const char *content,
                       int length) {
  uv_read_stop((uv_stream_t*)&conn->uv_tcp);
  
  kstring_t head = ks_null();
  
#define HTTP_1_1_ "HTTP/1.1 "
#define HTTP_SP " "
#define HTTP_LF "\r\n"
#define HTTP_DATE "Date: "
#define HTTP_SERVER "Server: "
#define HTTP_ALLOW "Allow: GET, POST, PUT, DELETE"
#define HTTP_CTYPE "Content-Type: "
#define HTTP_CLEN "Content-Length: "
  
  kputsc(HTTP_1_1_, &head);
  kputuw(status, &head);
  kputsc(HTTP_SP, &head);
  kputs(message, &head);
  kputsc(HTTP_LF, &head);
  
  /* add date header */
  {
    time_t t = time(NULL);
    struct tm *g = gmtime(&t);
    char s[sizeof(HTTP_DATE) - 1 + 32 + sizeof(HTTP_LF) - 1];
    
    strftime(s, sizeof(s)-1, HTTP_DATE "%a, %d %b %Y %H:%M:%S GMT" HTTP_LF, g);
    kputs(s, &head);
  }
  
  /* add server header */
  kputsc("Server: " LINKO_PROGRAM_STRING "/" LINKO_VERSION_STRING HTTP_LF, &head);
  
  /* adds body for html output */
  if (!content && conn->accept_type == MIME_HTML) {
    kstring_t body = ks_null();
    
    kputsc("<html><body><p>", &body);
    kputs(message, &body);
    kputsc("</p></body></html>", &body);
    
    mimetype = content_type[conn->accept_type];
    content = ks_str(&body);
    length = ks_len(&body);
  }

  /* add content type header */
  if (mimetype) {
    kputsc(HTTP_CTYPE, &head);
    kputs(mimetype, &head);
    kputsc(HTTP_LF, &head);
  }
  
  /* add content length header */
  if (length > 0) {
    kputsc(HTTP_CLEN, &head);
    kputuw(length, &head);
    kputsc(HTTP_LF, &head);
  }

  /* add allow header */
  if (status == 405 || status == 501) {
    kputsc(HTTP_ALLOW, &head);
  }
  
  /* add empty line */
  kputsc(HTTP_LF, &head);

  /* queue head to send */
  uv_buf_t *buf = kl_pushp(uv_buf_t, &conn->write_queue);
  buf->base = ks_str(&head);
  buf->len = ks_len(&head);
  
  /* send head */
  do_write(conn);

  if (content) {
    /* queue body to send */
    buf = kl_pushp(uv_buf_t, &conn->write_queue);
    buf->base = (char*)content;
    buf->len = length;

    /* send body */
    do_write(conn);
  }
}

static int on_url(http_parser* parser, const char *ptr, size_t len) {
  http_connection_t* conn = parser->data;
  
  kputsn(ptr, len, &conn->buffer);

  if (parser->http_major > 0 || parser->http_minor > 0)
    return 1;

  dlog("rest method `%s`", http_method_str(parser->method));
  dlog("rest url `%s`", ks_str(&conn->buffer));
  
  struct http_parser_url url;
  
  http_parser_parse_url(ks_str(&conn->buffer), ks_len(&conn->buffer), 0, &url);

  /* get node by path */
  if (url.field_set & (1 << UF_PATH)) {
    char *p = ks_str(&conn->buffer) + url.field_data[UF_PATH].off;
    char *e = p + url.field_data[UF_PATH].len;
    char *n;
    if (*p == '/') p++;
    
    conn->node = (node_t*)conn->service->root;

    if (p == e) goto done;
  node:
    n = strchr(p, '/');
    if (!n || n > e) n = e;
    
    if (n < e) { /* path nodes */
      *n = '\0';
      urldecode(p, p);
      
      dlog("rest get node `%s`", p);
      conn->node = node_get(conn->node, p);
      
      if (conn->node) {
        p = n + 1;
        goto node;
      }
    } else { /* last path node */
      *n = '\0';
      urldecode(p, p);
      
      if (!conn->node)
        return 0;

      node_t *parent = conn->node;

      dlog("rest get node `%s`", p);
      conn->node = node_get(conn->node, p);
      
      if (parser->method == HTTP_PUT && !conn->node) {
        dlog("rest add node `%s`", p);
        conn->node = node_add(parent, p);
      }
    }
  }
  
 done:
  ks_cut(&conn->buffer, 0);
  
  return 0;
}

static mime_type_t get_mime_type(const char *ptr, size_t len, mime_type_t def) {
#define CMP(s) (len >= (sizeof(s)-1) && *ptr == s[0] && 0 == strncmp(s, ptr, sizeof(s)-1))
  
  if (CMP("*")) {
    return MIME_HTML;
  } else if (CMP("application/")) {
    ptr += sizeof("application/")-1;
    len -= sizeof("application/")-1;
    
    if (CMP("x-yaml") || CMP("yaml")) {
      return MIME_YAML;
    } else if (CMP("json")) {
      return MIME_JSON;
    } else {
      return MIME_NONE;
    }
  } else if (CMP("text/")) {
    ptr += sizeof("text/")-1;
    len -= sizeof("text/")-1;
    
    if (CMP("html")) {
      return MIME_HTML;
    } else {
      return MIME_NONE;
    }
  } else {
    return MIME_NONE;
  }
  
#undef CMP
  return def;
}

static void set_header_value(http_connection_t *conn) {
  const char *ptr = ks_str(&conn->buffer);

  if (!ptr) return;
  
  size_t len = ks_len(&conn->buffer);
  
  dlog("rest header value `%s`", ptr);
  
  switch (conn->last_header) {
  case HEADER_ACCEPT:
    conn->accept_type = get_mime_type(ptr, len, conn->accept_type);
    break;
  case HEADER_CONTENT_TYPE:
    conn->content_type = get_mime_type(ptr, len, conn->content_type);
    break;
  case HEADER_NONE:
    break;
  }
  
  conn->last_header = HEADER_NONE;
}

static void set_header_field(http_connection_t *conn) {
  const char *ptr = ks_str(&conn->buffer);
  size_t len = ks_len(&conn->buffer);
  
  dlog("rest header field `%s`", ptr);
  
#define HH(a, A, h, H)                  \
  case a: case A:                       \
    if (sizeof(h)-1 == len &&           \
        0 == strncasecmp(h, ptr, len))  \
      conn->last_header = HEADER_##H;   \
    break
  
  switch (*ptr) {
    HH('a', 'A', "Accept", ACCEPT);
    HH('c', 'C', "Content-Type", CONTENT_TYPE);
  }
#undef HH
}

static int on_header_field(http_parser* parser, const char *ptr, size_t len) {
  http_connection_t* conn = parser->data;
  
  if (!conn->header_field) {
    set_header_value(conn);
    
    conn->header_field = true;
    
    ks_cut(&conn->buffer, 0);
  }
  
  kputsn(ptr, len, &conn->buffer);
  
  return 0;
}

static int on_header_value(http_parser* parser, const char *ptr, size_t len) {
  http_connection_t* conn = parser->data;
  
  if (conn->header_field) {
    set_header_field(conn);
    
    conn->header_field = false;
    
    ks_cut(&conn->buffer, 0);
  }
  
  kputsn(ptr, len, &conn->buffer);
  
  return 0;
}

static int on_body(http_parser* parser, const char *ptr, size_t len) {
  http_connection_t *conn = parser->data;
  
  http_service_t *service = conn->service;
  
  kstring_t buf = ks_null();
  
  kputsn(ptr, len, &buf);
  
  dlog("body chunk: `%s`", ks_str(&buf));
  
  free(ks_str(&buf));
  
  int r = json_tokenizer_execute(&conn->json_tokenizer,
                                 &service->json_tokenizer_settings,
                                 ptr, len);
  
  if (r != json_tokenizer_success)
    return 1;
  
  return 0;
}

static void get_node_url(node_t *node, kstring_t *path) {
  node_t *parent = node_parent(node);
  
  if (parent) {
    get_node_url(parent, path);
    
    kputc('/', path);
    kputs_urlencode(node_id(node), path);
  }
}

static void get_node_breadcrumb(node_t *node, kstring_t *html) {
  node_t *parent = node_parent(node);
  
  if (parent) {
    get_node_breadcrumb(parent, html);
    
    kputsc(" / <a href=\"", html);
    get_node_url(node, html);
    kputsc("\">", html);
    kputs(node_id(node), html);
    kputsc("</a>", html);
  } else {
    kputsc("<a href=\"/\">[root]</a>", html);
  }
}

static void on_get(http_connection_t* conn) {
  node_t *node = conn->node;
  int key;
  kstring_t body = { 0, 0, NULL };
  
  if (!node) {
    do_respond(conn, 404, "Not Found", NULL, NULL, 0);
    return;
  }
  
  switch (conn->accept_type) {
  case MIME_NONE:
    break;
  case MIME_JSON:
    kputsc("{\"children\":[", &body);
    
    if (none != (key = node_next(node, none))) {
      for (; ; ) {
        kputc('"', &body);
        kputs_strescape(node_name(node, key), &body);
        kputc('"', &body);
        
        if (none != (key = node_next(node, key)))
          kputc(',', &body);
        else
          break;
      }
    }
    
    kputsc("],\"property\":{", &body);
    
    if (none != (key = prop_next(node, none))) {
      for (; ; ) {
        const prop_info_t *info = prop_info(node, key);
        char *val = prop_get(node, key);
        
        kputc('"', &body);
        kputs_strescape(info->name, &body);
        kputsc("\":{", &body);
        
        kputsc("\"type\":\"", &body);
        kputs(info->type, &body);
        kputsc("\",\"help\":\"", &body);
        kputs_strescape(info->help, &body);
        kputsc("\",\"value\":", &body);
        
        if (val) {
          kputc('"', &body);
          kputs_strescape(val, &body);
          kputc('"', &body);
          
          free(val);
        } else {
          kputsc("null", &body);
        }
        
        kputc('}', &body);
        
        if (none != (key = prop_next(node, key)))
          kputc(',', &body);
        else
          break;
      }
    }
    
    kputsc("}}", &body);
    
    break;
  case MIME_YAML:
    if (none != node_next(node, none)) {
      kputsc("children:\n", &body);
      
      for (key = none; none != (key = node_next(node, key)); ) {
        kputsc("  - ", &body);
        kputs(node_name(node, key), &body);
        kputc('\n', &body);
      }
    }
    
    if (none != (key = prop_next(node, none))) {
      kputsc("property:\n", &body);
      
      for (; none != key; key = prop_next(node, key)) {
        const prop_info_t *info = prop_info(node, key);
        char *val = prop_get(node, key);
        
        kputsc("  ", &body);
        kputs(info->name, &body);
        kputsc(":\n    type: ", &body);
        kputs(info->type, &body);
        kputsc("\n    help: ", &body);
        kputs(info->help, &body);
        kputc('\n', &body);
        
        if (val) {
          kputsc("    value: ", &body);
          kputs(val, &body);
          kputc('\n', &body);
          
          free(val);
        }
      }
    }
    
    break;
  case MIME_HTML:
    {
      kputsc("<html><body>", &body);
      
      get_node_breadcrumb(node, &body);
      
      kputsc("<p>Children: ", &body);
      
      if (none != (key = node_next(node, none))) {
        kputsc("<ul>", &body);
        
        for (; none != key; key = node_next(node, key)) {
          kputsc("<li><a href=\"", &body);
          get_node_url(node_node(node, key), &body);
          kputsc("\">", &body);
          kputs(node_name(node, key), &body);
          kputsc("</a></li>", &body);
        }
        
        kputsc("</ul>", &body);
      } else {
        kputsc("none", &body);
      }
      
      kputsc("</p><p>Properties: ", &body);
      
      if (none != (key = prop_next(node, none))) {
        kputsc("<dl>", &body);
        
        for (; none != key; key = prop_next(node, key)) {
          const prop_info_t *info = prop_info(node, key);
          char *val = prop_get(node, key);
          
          kputsc("<dt><strong>", &body);
          kputs(info->type, &body);
          kputsc("</strong> <abbr title=\"", &body);
          kputs(info->help, &body);
          kputsc("\">", &body);
          kputs(info->name, &body);
          kputsc("</abbr></dt><dd>", &body);
          
          if (val) {
            kputs(val, &body);
            free(val);
          } else {
            kputsc("<s>none</s>", &body);
          }
          
          kputsc("</dd>", &body);
        }
        
        kputsc("</dl>", &body);
      }
      
      kputsc("</p></body></html>", &body);
    }
    
    break;
  }
  
  do_respond(conn, 200, "OK", content_type[conn->accept_type], ks_str(&body), ks_len(&body));
}

static void on_delete(http_connection_t* conn) {
  if (!conn->node) {
    do_respond(conn, 404, "Not Found", NULL, NULL, 0);
    return;
  }
  
  node_t *parent = node_parent(conn->node);

  if (!parent) {
    do_respond(conn, 406, "Parent node", NULL, NULL, 0);
    return;
  }
  
  if (node_del(parent, node_id(conn->node))) {
    do_respond(conn, 202, "Accepted", NULL, NULL, 0);
    return;
  }
}

static void on_put(http_connection_t* conn) {
  if (!conn->node) {
    do_respond(conn, 404, "Not Found", NULL, NULL, 0);
    return;
  }
  
  switch (conn->content_type) {
  case MIME_JSON:
  case MIME_YAML:
  case MIME_HTML:
    do_respond(conn, 204, "Updated", NULL, NULL, 0);
    break;
  default:
    do_respond(conn, 406, "Unsupported content type", NULL, NULL, 0);
  }
}

static int on_headers_complete(http_parser* parser) {
  http_connection_t* conn = parser->data;

  if (!conn->header_field) {
    set_header_value(conn);
    
    ks_cut(&conn->buffer, 0);
  }
  
  if (conn->accept_type == MIME_NONE) {
    do_respond(conn, 406, "Not Acceptable", NULL, NULL, 0);
    return 1;
  }
  
  switch (parser->method){
  case HTTP_GET:
    on_get(conn);
    return 1;
  case HTTP_PUT:
    if (conn->node) {
      json_tokenizer_init(&conn->json_tokenizer);
      conn->json_tokenizer.data = conn;
      return 0;
    } else {
      on_put(conn);
      return 1;
    }
  case HTTP_DELETE:
    on_delete(conn);
    return 1;
  }
  
  do_respond(conn, 405, "Method Not Allowed", NULL, NULL, 0);
  return 1;
}

static int on_message_complete(http_parser *parser) {
  http_connection_t* conn = parser->data;
  
  switch (parser->method){
  case HTTP_PUT:
    on_put(conn);
    break;
  }
  
  return 0;
}

static void select_prop(http_connection_t *conn) {
  strunescape(ks_str(&conn->buffer), ks_str(&conn->buffer));
  
  dlog("rest prop find `%s`", ks_str(&conn->buffer));
  
  conn->prop = prop_find(conn->node, ks_str(&conn->buffer));
  
  ks_cut(&conn->buffer, 0);
}

static void set_prop_value(http_connection_t *conn) {
  if (conn->prop == none)
    return;
  
  strunescape(ks_str(&conn->buffer), ks_str(&conn->buffer));
  
  dlog("rest prop set `%s`", ks_str(&conn->buffer));
  
  prop_set(conn->node, conn->prop, ks_str(&conn->buffer));

  ks_cut(&conn->buffer, 0);
  
  conn->prop = none;
}

static int on_object_open(json_tokenizer *tokenizer) {
  http_connection_t *conn = tokenizer->data;
  
  if (conn->in_object)
    return 0;
  
  conn->in_object = 1;
  
  return 1;
}

static int on_object_close(json_tokenizer *tokenizer) {
  http_connection_t *conn = tokenizer->data;

  if (!conn->in_object)
    return 0;
  
  set_prop_value(conn);
  
  conn->in_object = 0;
  
  return 1;
}

static int on_array_open(json_tokenizer *tokenizer) {
  (void)tokenizer;
  
  return 0;
}

static int on_array_close(json_tokenizer *tokenizer) {
  (void)tokenizer;
  
  return 0;
}

static int on_kv_separate(json_tokenizer *tokenizer) {
  http_connection_t *conn = tokenizer->data;
  
  select_prop(conn);
  
  return 1;
}

static int on_el_separate(json_tokenizer *tokenizer) {
  http_connection_t *conn = tokenizer->data;
  
  set_prop_value(conn);
  
  return 1;
}

static int on_atom_chunk(json_tokenizer *tokenizer, const char *ptr, size_t len) {
  http_connection_t *conn = tokenizer->data;
  
  kputsn(ptr, len, &conn->buffer);
  
  return 1;
}

static void rest_root_init(root_t *root, int addon) {
  http_service_t *service = root_addon_ptr(root, addon);
  
  service->root = root;
  uv_ip4_addr("0.0.0.0", 3030, &service->bind_addr);
  
  http_parser_settings *hs = &service->http_parser_settings;
  memset(hs, 0, sizeof(*hs));
  
  hs->on_url = on_url;
  hs->on_header_field = on_header_field;
  hs->on_header_value = on_header_value;
  hs->on_headers_complete = on_headers_complete;
  hs->on_body = on_body;
  hs->on_message_complete = on_message_complete;

  json_tokenizer_settings *js = &service->json_tokenizer_settings;
  memset(js, 0, sizeof(*js));

  js->on_object_open = on_object_open;
  js->on_object_close = on_object_close;
  js->on_array_open = on_array_open;
  js->on_array_close = on_array_close;
  js->on_kv_separate = on_kv_separate;
  js->on_el_separate = on_el_separate;
  js->on_null_chunk = on_atom_chunk;
  js->on_bool_chunk = on_atom_chunk;
  js->on_number_chunk = on_atom_chunk;
  js->on_string_chunk = on_atom_chunk;
  
  service->uv_tcp.data = service;
  
  safe_uv(uv_tcp_init(root_loop(service->root), &service->uv_tcp));
  safe_uv(uv_tcp_bind(&service->uv_tcp, (const struct sockaddr*)&service->bind_addr, 0));
  
  service->running = true;
  service->connections = 0;

  kmp_prepare(conpool, &service->con_pool);
  kmp_prepare(bufpool, &service->buf_pool);
  
  ilog("rest init");
  
  uv_listen((uv_stream_t*)&service->uv_tcp, 128, on_connect);
}

static void rest_root_done(root_t *root, int addon) {
  (void)root;
  http_service_t *service = root_addon_ptr(root, addon);
  
  service->running = false;
  service->stopped = false;
  
  ilog("rest done");
  
  if (!service->connections)
    uv_close((uv_handle_t*)&service->uv_tcp, on_stop);
  
  uv_loop_t *loop = root_loop(root);
  
  for (; !service->stopped; )
    uv_run(loop, UV_RUN_NOWAIT);
}

addon_vmt_t addon_symbol(rest) = {
  "rest",
  "Starts server to interact with linko through HTTP REST API.",
  
  sizeof(http_service_t),
  
  rest_root_init,
  rest_root_done,
  
  0,
  
  NULL,
  NULL,

  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
};
