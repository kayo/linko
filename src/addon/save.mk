BUILD:=plugin
save.backend?=filesystem

SRC:=$(wildcard $(ADDON_DIR)/$(NAME)/$(save.backend).c)

ifeq (,$(SRC))
  $(error Invalid save addon backend: $(save.backend). Available backends is: $(patsubst $(ADDON_DIR)/$(NAME)/%.c,%,$(wildcard $(ADDON_DIR)/$(NAME)/*.c)))
endif

ifeq (sqlite3,$(save.backend))
  LIBS:=-lsqlite3
endif
