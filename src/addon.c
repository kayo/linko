#include "common.h"
#include "node.h"
#include "addon.h"

#ifdef ADDONS_PLUGINS
#include <dirent.h>
#endif/*ADDONS_PLUGINS*/

#include <kvec.h>

typedef struct {
  addon_vmt_t *vmt;
  size_t root_offset;
  size_t node_offset;
  bool plugin;
  uv_lib_t uv_lib;
} addon_data_t;

typedef kvec_t(addon_data_t) addon_list_t;

struct _addons {
  addon_list_t list;
};

#ifdef ADDONS_BUILTIN
#define addon(name) extern addon_vmt_t addon_##name##_vmt;
ADDONS_BUILTIN
#undef addon
#endif/*ADDONS_BUILTIN*/

#ifdef ADDONS_PLUGINS
static int plugin_filter(const struct dirent *ent){
  char *ext = strrchr(ent->d_name, '.');
  
  if (!ext)
    return 0;
  
  if (strcmp(".so", ext) && strcmp(".dll", ext))
    return 0;
  
  return 1;
}

static void addon_init(addons_t *addons, addon_data_t *a, bool plugin) {
  a->plugin = plugin;
  
  if (a > addons->list.a) {
    addon_data_t *p = a - 1;
    
    a->root_offset = p->root_offset + p->vmt->root_size;
    a->node_offset = p->node_offset + p->vmt->node_size;
  } else {
    a->root_offset = 0;
    a->node_offset = 0;
  }
}

static void addons_plug(addons_t *addons, const char *dir){
  struct dirent **list;
  int n = scandir(dir, &list, plugin_filter, alphasort);
  
  if (n < 0)
    return;
  
  struct dirent **ptr = list, **end = list + n;
  
  for (; ptr < end; ptr++) {
    addon_data_t *a = kv_pushp(addon_data_t, addons->list);

    int dir_len = strlen(dir);
    
    char *file = (*ptr)->d_name;
    int file_len = strlen(file);
    
    char *path = malloc(dir_len + 1 + file_len + 1);
    
    memcpy(path, dir, dir_len);
    path[dir_len] = '/';
    memcpy(path + dir_len + 1, file, file_len);
    path[dir_len + 1 + file_len] = '\0';
    
    if (0 != uv_dlopen(path, &a->uv_lib)) {
      elog("Plugin error: %s", uv_dlerror(&a->uv_lib));
      return;
    }
    
    free(path);
    
    int len = strrchr(file, '.') - file;

#define P(s) #s
#define S(s) P(s)
#define L(s) (sizeof(S(s))-1)
    
    char *sym = malloc(L(addon_prefix) + len + L(addon_suffix) + 1);
    
    memcpy(sym, S(addon_prefix), L(addon_prefix));
    memcpy(sym + L(addon_prefix), file, len);
    memcpy(sym + L(addon_prefix) + len, S(addon_suffix), L(addon_suffix));
    sym[L(addon_prefix) + len + L(addon_suffix)] = '\0';

#undef P
#undef S
#undef L
    
    if (0 != uv_dlsym(&a->uv_lib, sym, (void**)&a->vmt)) {
      elog("Plugin error: %s", uv_dlerror(&a->uv_lib));
      return;
    }
    
    free(sym);
    
    addon_init(addons, a, true);
    
    free(*ptr);
  }
  
  free(list);
}
#endif/*ADDONS_PLUGINS*/

size_t addons_size(void){
  return sizeof(addons_t);
}

void addons_init(addons_t *addons){  
  kv_init(addons->list);
  
  /* add builtin addons */
#ifdef ADDONS_BUILTIN
#define addon(name) {                                         \
    addon_data_t *a = kv_pushp(addon_data_t, addons->list);   \
    a->vmt = &addon_symbol(name);                             \
    addon_init(addons, a, false);                             \
  }
  ADDONS_BUILTIN
#undef addon
#endif/*ADDONS_BUILTIN*/

  /* plugin extra addons */
#ifdef ADDONS_PLUGINS
  char *path = getenv("LINKO_PLUGIN_PATH");
  
  if (!path)
    path = (char*)LINKO_PLUGIN_PATH;
  
  path = strdup(path);
  
  char *dir = path, *end;
  
  for (; ; ) {
    end = strchr(dir, ':');
    if (end) *end = '\0';
    
    addons_plug(addons, dir);
    
    if (end) dir = end + 1;
    else break;
  }
  
  free(path);
#endif/*ADDONS_PLUGINS*/
}

void addons_done(addons_t *addons){
  size_t i;

  for (i = 0; i < kv_size(addons->list); i++) {
    addon_data_t *a = &kv_A(addons->list, i);

    if (a->plugin)
      uv_dlclose(&a->uv_lib);
  }
  
  kv_destroy(addons->list);
}

int addons_find(const addons_t *addons, const char *name){
  size_t i;
  
  for (i = 0; i < kv_size(addons->list); i++) {
    addon_data_t *a = &kv_A(addons->list, i);

    if (0 == strcmp(a->vmt->name, name))
      return i;
  }
  
  return none;
}

int addons_next(const addons_t *addons, int prev){
  return prev < (int)kv_size(addons->list) - 1 ? prev + 1 : none;
}

const addon_vmt_t *addons_vmt(const addons_t *addons, int key){
  assert(0 <= key && key < (int)kv_size(addons->list));
  
  return kv_A(addons->list, key).vmt;
}

size_t addons_root_size(const addons_t *addons){
  if (kv_size(addons->list) < 1) return 0;
  
  addon_data_t *last = &kv_A(addons->list, kv_size(addons->list) - 1);
  
  return last->root_offset + last->vmt->root_size;
}

size_t addons_root_offset(const addons_t *addons, int key){
  assert(0 <= key && key < (int)kv_size(addons->list));
  
  return kv_A(addons->list, key).root_offset;
}

size_t addons_node_size(const addons_t *addons){
  if (kv_size(addons->list) < 1) return 0;
  
  addon_data_t *last = &kv_A(addons->list, kv_size(addons->list) - 1);
  
  return last->node_offset + last->vmt->node_size;
}

size_t addons_node_offset(const addons_t *addons, int key){
  assert(0 <= key && key < (int)kv_size(addons->list));
  
  return kv_A(addons->list, key).node_offset;
}
