/**
 * @brief The addons object.
 */
typedef struct _addons addons_t;

/**
 * @brief The addon virtual method table.
 */
typedef struct {
  const char *name;
  const char *info;

  /**
   * @brief The root addon size.
   */
  size_t root_size;
  
  /**
   * @brief Root addon init.
   */
  void (*root_init)(root_t *root, int addon);
  
  /**
   * @brief Root addon done.
   */
  void (*root_done)(root_t *root, int addon);
  
  /**
   * @brief The node addon size.
   */
  size_t node_size;
  
  /**
   * @brief Node addon init.
   */
  void (*node_init)(node_t *node, int addon);
  
  /**
   * @brief Node addon done.
   */
  void (*node_done)(node_t *node, int addon);
  
  /**
   * @brief Iterate over properties.
   */
  int (*prop_next)(node_t *node, int addon, int prev);
  
  /**
   * @brief Find property by name.
   */
  int (*prop_find)(node_t *node, int addon, const char *name);
  
  /**
   * @brief Get property info.
   */
  const prop_info_t *(*prop_info)(node_t *node, int addon, int curr);
  
  /**
   * @brief Get property value as string.
   */
  char *(*prop_get)(node_t *node, int addon, int key);

  /**
   * @brief Set property value from string.
   */
  void (*prop_set)(node_t *node, int addon, int key, const char *val);

  /**
   * @brief Trigger property value changed.
   */
  void (*prop_trig)(node_t *node, int addon, int key);
} addon_vmt_t;

size_t addons_size(void);
void addons_init(addons_t *addons);
void addons_done(addons_t *addons);

static inline addons_t *addons_new(void){
  addons_t *addons = malloc(addons_size());
  addons_init(addons);
  return addons;
}

static inline void addons_del(addons_t *addons){
  addons_done(addons);
  free(addons);
}

int addons_find(const addons_t *addons, const char *name);
int addons_next(const addons_t *addons, int prev);

const addon_vmt_t *addons_vmt(const addons_t *addons, int key);

size_t addons_root_size(const addons_t *addons);
size_t addons_root_offset(const addons_t *addons, int key);

size_t addons_node_size(const addons_t *addons);
size_t addons_node_offset(const addons_t *addons, int key);

#define addon_prefix addon_
#define addon_suffix _vmt

#define addon_concat_symbol(pfx, sym, sfx) pfx##sym##sfx
#define addon_wrap_symbol(pfx, sym, sfx) addon_concat_symbol(pfx, sym, sfx)
#define addon_symbol(name) addon_wrap_symbol(addon_prefix, name, addon_suffix)
