ADDON_DIR?=addon
ADDONS?=$(patsubst $(ADDON_DIR)/%.mk,%,$(wildcard $(ADDON_DIR)/*.mk))

ifneq (,$(plugin.path))
DEF+=LINKO_PLUGIN_PATH="\"$(plugin.path)\""
endif

build: build.plugins

clean: clean.plugins

define addon_config

global.NAME:=$(NAME)
global.SRC:=$(SRC)
global.CFLAGS:=$(CFLAGS)
global.LDFLAGS:=$(LDFLAGS)
global.LIBS:=$(LIBS)

# none|builtin|plugin
NAME:=$(1)
BUILD:=plugin
SRC:=$(wildcard $(ADDON_DIR)/$(1).c $(ADDON_DIR)/$(1)-*.c $(ADDON_DIR)/$(1)/*.c)
CFLAGS:=
LDFLAGS:=
LIBS:=

include $(ADDON_DIR)/$(1).mk

addon.$(1).SRC:=$$(SRC)
addon.$(1).CFLAGS:=$$(CFLAGS)
addon.$(1).LDFLAGS:=$$(LDFLAGS)
addon.$(1).LIBS:=$$(LIBS)

define addon_compile
addon.$(1).OBJ+=$$(1).o
$$(1).o: $$(1).c
	@echo ADDON $(1) CC $$$$@
	$$$$(E)$$$$(CC) -c $$$$(CFLAGS) $$$$(addon.$$(1).CFLAGS) -o $$$$@ $$$$<
endef

$$(foreach s,$$(patsubst %.c,%,$$(SRC)),$$(eval $$(call addon_compile,$$(s))))

ifeq (builtin,$$(BUILD))
BUILTIN+=$(1)
OBJ+=$$(addon.$(1).OBJ)
global.LDFLAGS+=$$(addon.$(1).LDFLAGS)
global.LIBS+=$$(addon.$(1).LIBS)
endif

ifeq (plugin,$$(BUILD))
PLUGINS+=$(1)

build.plugins: build.plugin.$(1)

build.plugin.$(1): $(ADDON_DIR)/$(1).so

$(ADDON_DIR)/$(1).so: $$(addon.$(1).OBJ)
	@echo ADDON $(1) LD $$@ USING $$^
	$$(E)$$(CC) -shared -o $$@ $$(LDFLAGS) $$(addon.$(1).LDFLAGS) $$(addon.$(1).LIBS) $$^

clean.plugins: clean.plugin.$(1)

clean.plugin.$(1):
	@echo ADDON $(1) CLEAN
	$$(E)rm -f $$(addon.$(1).OBJ) $(ADDON_DIR)/$(1).so

endif

NAME:=$$(global.NAME)
SRC:=$$(global.SRC)
CFLAGS:=$$(global.CFLAGS)
LDFLAGS:=$$(global.LDFLAGS)
LIBS:=$$(global.LIBS)

global.NAME:=
global.SRC:=
global.CFLAGS:=
global.LDFLAGS:=
global.LIBS:=

endef #addon_config

BUILTIN:=
PLUGINS:=

$(foreach a,$(ADDONS),$(eval $(call addon_config,$(a))))

LDFLAGS+=-rdynamic

DEF+=ADDONS_PLUGINS=1
DEF+=ADDONS_BUILTIN="$(subst $(eval) ,,$(patsubst %,addon(%),$(BUILTIN)))"
